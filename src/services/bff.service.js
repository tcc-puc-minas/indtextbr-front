import http from '../config/http';
import axios from 'axios';
import {getToken} from '../config/auth'

const bff = () => {
    const token = getToken();
    const req = axios.create({
        baseURL: 'https://indtextbr-bff.herokuapp.com/v1',
        headers: {
            'content-type': 'application/json',
            token: token
        }
    })

    return req;
}

const login = async(data) => {
    const response = bff().post('/login', data)
    return response;
};

const getOrders = () => bff().get('/sgi/orders');

const getOrdersById = (id) => bff().get(`/sgi/orders/${id}`);

const changeOrderPriority = (id, data) => bff().put(`/sgi/orders/${id}/priority`, data);

const createUser = (data) => bff().post('/user', data);

const getUsers = () => bff().get('/user');

const setUserStatus = (id, data) => bff().put(`/user/${id}`, data);


const getStandards = () => bff().get('/sgn/standards');

export {
    login,
    getOrders,
    getOrdersById,
    changeOrderPriority,
    createUser,
    getUsers,
    setUserStatus,
    getStandards
}

