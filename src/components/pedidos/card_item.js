import { Link } from 'react-router-dom';
import {
    Button,
    Card, CardBody,
    CardTitle, CardSubtitle, 
    CardText
} from 'reactstrap';
import styled from 'styled-components';
import {capitalize} from '../../utils/format.utils';

const CardItem = (props) => {
    const { item, purchaser, id, status, quantity, priority, color } = props.item;


    const clientNickname = () => {
        const arr = purchaser.split(' ');
        if(arr.length < 1){
            return arr[0];
        }else{
            return `${arr[0]} ${arr[1]}` 
        }
    }
    

    return (
        <SCard>
            <CardBody>
                <CardTitle style={titleStyle} tag="h5"> <b>{item} - {color}</b> </CardTitle>
                <CardSubtitle style={titleStyle} tag="h6"> {quantity} </CardSubtitle>
                <br /> 
                <CardText>
                <i>Cliente: </i><b>{clientNickname()}</b><br />
                <i>Qtd: </i><b>{quantity}</b><br />
                <i>Prioridade: </i><b style={priorityColors(priority)}>{capitalize(priority)}</b><br />
                <div style={{...titleStyle, marginTop: 5}}>{status}</div>
                </CardText>
                <CardText style={{textAlign: 'center'}}><Button style={{background: '#4B8EC7'}}  size="sm" tag={Link} to={`/sgi/pedidos/detalhes/${id}`} >Ver detalhes</Button></CardText>
                {/* <CardText style={{textAlign: 'center'}}><Button style={{background: '#4B8EC7'}}  size="sm" onClick={() => history.push(`/sgi/detalhes/${id}`)} >Ver detalhes</Button></CardText> */}
                
            </CardBody>
        </SCard>
    )
}

export default CardItem;



const SCard =  styled(Card)`

    background-color: #fafafa;
    display: flex;
    :hover {
        background-color: #ddd;
        transition:1s;
        opacity: 0.9;
    }
`


const priorityColors = (priority) => {
    let color;
    switch (priority) {
        case 'baixa':
            color = '#17a2b8'
            break;
    
        case 'media':
            color = '#cea014'
            break;
        
        case 'alta':
            color = '#dc3545'
            break;
    }
    return {color: color};
}
const titleStyle = {
    color: '#053566', textAlign: 'center'
}