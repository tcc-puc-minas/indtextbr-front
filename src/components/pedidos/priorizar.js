import { useState } from "react";
import { changeOrderPriority } from "../../services/bff.service";
import { Button, Row, Col, FormGroup, Form, Label, Input } from "reactstrap";
import styled from "styled-components";
import ReactSwal from "../../plugins/swal";

const Priorizar = ({ id, currentPriority, update, isForm }) => {
  const initalFormState = {
    priority: currentPriority,
  };
  const [form, setForm] = useState(initalFormState);

  const handleChange = (e) => {
    setForm({
      ...form,
      [e.target.name]: e.target.value,
    });
  };

  const submitForm = () => {
    const nform = {
      ...form,
      priority: form.priority,
    };

    changeOrderPriority(id, nform)
      .then(() => {
        ReactSwal.fire({
          icon: "success",
          title: `Priorização do pedido foi definida para ${form.priority}!`,
          showConfirmButton: false,
          showCloseButton: true,
        });
        update(true);
        isForm(false);
      })
      .catch((erro) => {
        console.error(erro);
        ReactSwal.fire({
          icon: "error",
          title: `Erro ao alterar priorização do pedido!`,
          showConfirmButton: false,
          showCloseButton: true,
        });
      });
  };

  return (
    <BoxPriority>
      <Col xs="12" sm="12" md="8" lg="8">
        <Form inline style={{width: '100%'}}>
        <FormGroup style={{marginRight: '10px',  minWidth: '50%'}}>
          <Input
          style={{width: '100%'}}
            type="select"
            name="priority"
            id="priority"
            value={form.priority}
            onChange={handleChange}
          >
            <option value="baixa">Baixa</option>
            <option value="media">Média</option>
            <option value="alta">Alta</option>
          </Input>
        </FormGroup>
        <FormGroup >
          <Button color="primary" onClick={submitForm}>
            Alterar
          </Button>
        </FormGroup>
        </Form>
        
      </Col>
    </BoxPriority>
  );
};

export default Priorizar;

const BoxPriority = styled(Row)``;
