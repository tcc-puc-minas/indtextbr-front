
import {
    Card, CardBody,
    CardTitle, CardSubtitle,
    CardText
} from 'reactstrap';
import styled from 'styled-components';

const CardItem = (props) => {
    const { title, qtd, limit, type, update_at } = props.item;
    let status;
    if(qtd < limit){
        status = 'Ótima'
    }else if(qtd > limit){
        status = 'Regular'
    }else{
        status = 'Ruim'
    }

    return (
        <SCard>
            <CardBody>
                <CardTitle style={titleStyle} tag="h6"> <b>{title}</b> </CardTitle>
                <CardSubtitle style={priorityColors(qtd, limit)} tag="h6"> Situação: {status} 
                <br /> </CardSubtitle>
                {/* <CardSubtitle style={priorityColors(qtd, limit)} tag="h6"> </CardSubtitle> */}
                <br />
                <CardText><i>Irregularides:</i> <b>{qtd} de {limit}</b> <br /> 
                    <i>Tipo:</i> <b>{type}</b>
                </CardText>
                <CardText style={{fontSize: '12px', textAlign: 'center'}}>
                <i >Atualizado em: {update_at}</i>
                </CardText>
                {/* <CardText style={{textAlign: 'center'}}><Button style={{background: '#4B8EC7'}}  size="sm" tag={Link} to={`/sgi/pedidos/detalhes/${id}`} >Ver detalhes</Button></CardText> */}
                {/* <CardText style={{textAlign: 'center'}}><Button style={{background: '#4B8EC7'}}  size="sm" onClick={() => history.push(`/sgi/detalhes/${id}`)} >Ver detalhes</Button></CardText> */}
                
            </CardBody>
        </SCard>
    )
}

export default CardItem;



const SCard =  styled(Card)`

    background-color: #fafafa;
    display: flex;
    :hover {
        background-color: #ddd;
        transition:1s;
        opacity: 0.9;
    }
`

const priorityColors = (qtd, limit) => {
    let color;
    if(qtd < limit){
        color = '#17a2b8'
    }else if(qtd > limit){
        color = '#cea014'
    }else{
        color = '#dc3545'
    }
    return {color: color, textAlign: 'center'};
}
const titleStyle = {
    color: '#053566', textAlign: 'center'
}