import React from "react";

const Title = ({ name}) => {

  return (
    <div className="mainTitle">
      <h3>{name}</h3>
    </div>
  );
};

export default Title;
