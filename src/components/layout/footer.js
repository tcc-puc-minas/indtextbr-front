import styled from "styled-components";

const Footer = () => (
    <SFooter>
        <p>Desenvolvido por: Ezer Assis e Eliezer Cútalo</p>
    </SFooter>
);

export default Footer;


const SFooter = styled.footer`
    background-color: #053566 !important;
    color: #fff;
    border-top: 2px solid #4b8EC7;
    text-align:center;
    padding: 10px;
`