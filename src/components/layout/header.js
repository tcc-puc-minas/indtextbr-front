import React, { useState } from 'react';
import {useDispatch} from 'react-redux';
import { signOutAction } from '../../store/auth/auth.action';
import { NavLink as RRDNavLink } from 'react-router-dom'; // funcionalidade
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink,    // estilo
    Container,
    Tooltip,
    Button,
} from 'reactstrap';
import styled from 'styled-components';
import { BsGraphUp, BsPower } from 'react-icons/bs'
import { useSelector } from 'react-redux';
const Header = (props) => {
    let usuario = useSelector(state => state.auth.usuario);
    const dispatch = useDispatch();
    const [isOpen, setIsOpen] = useState(false);
    const [tooltipOpen, setTooltipOpen] = useState(false);
    // const [isAuth, setIsAuth] = useState(false);

    const toggleTooltip = () => setTooltipOpen(!tooltipOpen);
    const sair = () => {
        dispatch(signOutAction());
    }

    const toggle = () => setIsOpen(!isOpen);
    return (
        <header>
            <SNavbar color="dark" dark expand="md">
                <Container>
                    <NavbarBrand tag={RRDNavLink} to="/" id="logoMain"> <IconLogo /> IndTextBR</NavbarBrand>
                    <Tooltip placement="top" isOpen={tooltipOpen} autohide={false} target="logoMain" toggle={toggleTooltip}>
                        Voltar ao Menu Principal
                    </Tooltip>
                    {Object.keys(usuario).length > 0 ? <> <NavbarToggler onClick={toggle} />
                    <SCollapse isOpen={isOpen} navbar>
                        <Nav className="mr-auto" navbar>
                            <NavItem>
                                <SNavLink exact tag={RRDNavLink} activeClassName="active" to="/sgi/pedidos" >Pedidos</SNavLink>
                            </NavItem>
                            <NavItem >
                                <SNavLink exact tag={RRDNavLink} activeClassName="active" to="/sgi/usuarios" >Usuarios</SNavLink>
                            </NavItem>
                            <NavItem >
                                <SNavLink exact tag={RRDNavLink} activeClassName="active" to="/sgn/normas" >Normas</SNavLink>
                            </NavItem>
                        </Nav>
                    </SCollapse>
                    <Button color="secondary" alt="Sair" onClick={sair} style={{fontWeight: "bold"}}> <BsPower  />{' '}  </Button> </>: <div></div>}
                </Container>
            </SNavbar>
        </header>
    )
}

export default Header


const SNavbar = styled(Navbar)`
    background-color: #053566 !important;
    border-bottom: 5px solid #4b8EC7;

    a {
        color: #fff !important;
    }

`

const SNavLink = styled(NavLink)`
    margin: auto 5px;
    border-radius: 5px;

    &.active {
        color: #fff !important;
        background-color: #4B8EC7 !important;
    }

    @media (max-width: 767.98px) {
        margin:6px 0;
        
    }

`
const SCollapse = styled(Collapse)`
    flex-grow: 0;
`

const IconLogo = styled(BsGraphUp)`
    font-size: 26px;
    margin-top: -4px
`