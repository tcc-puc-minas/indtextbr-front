import React from 'react';
import ReactDOM from 'react-dom';
import './assets/css/style.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import GlobalStyled from './assets/globalStyled'
import { Provider } from 'react-redux'
import store from './store'


// routers
import Routers from './routers'

ReactDOM.render(
  <Provider store={store}>
    <GlobalStyled />
    <Routers />
  </Provider>,
  document.getElementById('root')
);