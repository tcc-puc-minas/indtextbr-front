import React, { useState } from 'react';
import Loading from "../../components/loading";
import {
    Form, FormGroup, Input,
    Card, Col, CardBody,
    CardHeader,
    Button, Label, FormFeedback
} from 'reactstrap';
import { Sign } from '../../assets/styled';
import { signInAction, authorizeUserWithTokenAction } from '../../store/auth/auth.action'
import { useDispatch, useSelector } from 'react-redux';
import { getUserData, isAuthenticated } from '../../config/auth';
const SignIn = () => {
    const dispatch = useDispatch();
    if(isAuthenticated()){
        dispatch(authorizeUserWithTokenAction(getUserData()));
    }
    const [form, setForm] = useState({
        email: "",
        password: ""
    });

    const [loading, setLoading] = useState(false);
    const handleChange = (props) => {
        const { value, name } = props.target;
        
        setForm({
            ...form,
            [name]: value,
        });
    };
    let hasError = useSelector(state => state.auth.hasError);

    const isSubmit = () => form.email.trim() !== '' && form.password.trim() !== '';

    const submitForm = () => {
        setLoading(true);
        dispatch(signInAction(form));
    }

    // const valida

    return (
        <Sign>
            {loading && !hasError ? <Loading /> :
            <Col sm={12} md={4} lg={5}>
                <Card>
                    <CardHeader tag="h4" className="text-center">Login</CardHeader>
                    <CardBody>
                        
                        <Form>
                            
                            <FormGroup>
                                <Label for="email">E-mail:</Label>
                                <Input invalid={hasError} type="email" name="email" id="email" onChange={handleChange} onPaste={handleChange}  value={form.email } placeholder="Informe seu E-mail" />
                                
                            </FormGroup>
                            <FormGroup>
                                <Label for="password">Senha:</Label>
                                <Input invalid={hasError} type="password" name="password" id="password" onChange={handleChange} onPaste={handleChange} value={form.password } placeholder="Informe sua senha" />
                                <FormFeedback invalid>Usuário ou senha inválido</FormFeedback>
                            </FormGroup>

                            <Button color={!isSubmit() ? 'secondary' : 'primary'} disabled={!isSubmit()} size="sm" block onClick={submitForm}>
                                Entrar
                            </Button>
                            
                        </Form >

                    </CardBody>

                </Card>
            </Col> }
        </Sign>
    )
}

export default SignIn;

