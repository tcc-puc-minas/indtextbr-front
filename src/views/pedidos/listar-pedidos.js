import React, { useState, useEffect, useCallback } from "react";
import { getOrders } from "../../services/bff.service.js";
import CardItem from "../../components/pedidos/card_item";
import Loading from "../../components/loading";
import styled from "styled-components";
import { Col, Row } from "reactstrap";
import Title from '../../components/layout/title'
import history from "../../config/history";
const Pedidos = () => {
  const [pedidos, setPedidos] = useState([]);
  const [loading, setLoading] = useState(false);
  // const [hasError, setError] = useState(false)

  const getPedidos = useCallback(async () => {
    try {
      setLoading(true);
      const res = await getOrders();
      setPedidos(res.data);
      setLoading(false);
    } catch (error) {
      setLoading(false);
      if(error.response.status === 403){
        history.push('/error/403')
      }else{
        console.error(error);
      }
    }
  }, []);

  useEffect(() => {
    getPedidos();
  }, [getPedidos]);

  const MapearPedidos = (pedidos) =>
    pedidos.map((item, i) => (
      <Col md="3" xl="3" sm="12" xs="12" key={i} className="mb-4">
        <CardItem item={{ ...item }} />
      </Col>
    ));

  return (
    <div>
      <Title name={"Pedidos"}></Title>
      <BoxPedidos>{loading ? <Loading /> : MapearPedidos(pedidos)}</BoxPedidos>
    </div>
  );
};

export default Pedidos;

const BoxPedidos = styled(Row)``;
