import { useEffect, useState, useCallback } from "react";
import { useParams } from "react-router";
import { getOrdersById } from '../../services/bff.service'
import { Jumbotron, Button } from 'reactstrap';
import Loading from '../../components/loading'
import Priorizar from '../../components/pedidos/priorizar'
import Title from '../../components/layout/title';
import styled from "styled-components";
import { BsChevronCompactLeft,BsChevronCompactDown } from 'react-icons/bs'
import {capitalize} from '../../utils/format.utils';
import history from "../../config/history";
const Detalhes = (props) => {
    const { id } = useParams();
    const { history } = props;

    const [loading, setLoading] = useState(false);
    const [detalhe, setDetalhe] = useState({});
    const [update, setUpdate] = useState(false)
    const [isSub, setSub] = useState(false)

    const getDetalhes = useCallback(async () => {
        try {
            setLoading(true)
            const res = await getOrdersById(id);
            setDetalhe(res.data)
            setLoading(false)

        } catch (error) {
            if(error.status === 403){
                history.push('/error/403')
              }else{
                console.error(error);
              }
        }

    }, [id, history]);

    useEffect(() => {
        getDetalhes()
        setUpdate(false)
    }, [getDetalhes, update])

    const Detalhamento = ({ name, coordinator }) => (
        <Jumbotron>
            <div style={{color: '#053566'}} className="display-4"><b>{detalhe.item} {capitalize(detalhe.color)} <i>({detalhe.quantity})</i></b></div>
            <p className="lead"><i>Cliente solicitante:</i> {detalhe.purchaserName} | <span style={priorityColors(detalhe.priority)}>{capitalize(detalhe.priority) } prioridade</span></p>
            <hr className="my-2" />
                <p><i>Número do pedido:</i> {detalhe.id}</p>
                <p><i>Status:</i> {detalhe.status}</p>
                <p><i>Departamento responsável:</i> {detalhe.departmentName}</p>
                <p><i>Detalhamento:</i> {detalhe.details}</p>
                <br />
                <i>Data de atualização:</i> {detalhe.updateAt}<br /><br />

                <Menu></Menu>
                {

                isSub
                    ? (<Priorizar id={id} currentPriority={detalhe.priority} update={setUpdate} isForm={setSub} />)
                    : ''
                }
                <br />
                <Button color="secondary" onClick={()=> history.goBack()}>
                        Voltar
                    </Button>
        </Jumbotron>
    )


    const Menu = () => (
        <Navbar expand="md mb-4">
            <div className="info">
                {isSub ? "Qual será a nova prioridade?" : "Alterar prioridade"}
            </div>
            <Button onClick={() => setSub(!isSub)} size="sm">
               
                {!isSub ? (<><BsChevronCompactLeft /></>) : (<><BsChevronCompactDown /> </>)}
            </Button>
        </Navbar>
    )

    const montarTela = (detalhe) => (
        <div>
            <Title name="Detalhes do pedido"></Title>
            {Detalhamento(detalhe)}
           
        </div>
    )

    return (
        loading
            ? <Loading />
            : montarTela(detalhe)
    )
}


export default Detalhes;


const Navbar = styled.div`
    background-color:none !important;
    margin: 10px 0 20px;
    padding: 10px 0;
    border-bottom: thin dotted #4446;
    display:flex;
    
    .info {
        flex:1;
    }


`

const priorityColors = (priority) => {
    let color;
    switch (priority) {
        case 'baixa':
            color = '#17a2b8'
            break;
    
        case 'media':
            color = '#cea014'
            break;
        
        case 'alta':
            color = '#dc3545'
            break;
    }
    return {color: color};
}