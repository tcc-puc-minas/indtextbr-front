import React from 'react';

const Error404 = () => {


    return (
        <div className={"indInfo"}>
            <h2>Aconteceu um Erro</h2>
            <br />
            <h4>A página que você tentou acessar não existe ou você não tem permissão</h4>
            <h4>Em caso de duvidas, entre em contato com o administrador</h4>
            <br /> <br />
        </div>
    )
}

export default Error404;