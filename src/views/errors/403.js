const Error403 = () => {


    return (
        <div className="contentType">
            <h1>403</h1>
            <h2>Não autorizado!</h2>
            <h4>Desculpas, mas você não tem permissão para acessar essa página!</h4>
            <h6>Caso seja necessário, entre em contato com o administrador do sistema</h6>
        </div>
    )
}

export default Error403;