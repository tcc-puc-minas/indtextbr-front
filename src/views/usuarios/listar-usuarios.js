import React, { useState, useEffect, useCallback } from "react";
import {
  BsFillLockFill,
  BsFillUnlockFill,
  BsPersonPlusFill,
} from "react-icons/bs";
import Loading from "../../components/loading";
import {
  Table,
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Form,
  FormGroup,
  Input,
  Label,
  Card,
  CardHeader,
  CardBody,
} from "reactstrap";
import Title from "../../components/layout/title";
import { getUsers, setUserStatus } from "../../services/bff.service";
import history from "../../config/history";
import ReactSwal from "../../plugins/swal";
const Usuarios = () => {
  const [modal, setModal] = useState({
    isOpen: false,
    data: null,
  });

//   const [userCard, setUserCard] = useState(false);

  const [loading, setLoading] = useState(false);
  const [usuarios, setUsuarios] = useState([]);
  const [update, setUpdate] = useState(false)
  const toggleModal = (data = null) => {
    setModal({
      isOpen: !modal.isOpen,
      data,
    });
  };

  const alterarStatus = ({id, status}) => {
    // setLoading(true);
    setUserStatus(id, {status: !status})
      .then((res) => {
        ReactSwal.fire({
          icon: "success",
          title: `Usuário alterado com sucesso!`,
          showConfirmButton: false,
          showCloseButton: true,
          onClose: listarUsuarios()
        });
        toggleModal()
      })
      .catch((err) => {
        console.log(err);
        console.log("Meu err: ", JSON.stringify(err))
        if (err.response.status === 403) {
          history.push("/error/403");
        } else {
          console.error("Deu ruim");
        }
      });
  };

  const listarUsuarios = useCallback(async () => {
    setLoading(true);
    getUsers()
      .then((res) => {
        setUsuarios(res.data);
        setLoading(false);
        console.log('Cai aqui');
        setUpdate(false)
      })
      .catch((err) => {
        console.log(err);
        if (err.response.status === 403) {
          history.push("/error/403");
          setLoading(false);
        } else {
          console.error("Deu ruim");
          
        }
        setLoading(false);
      });
  }, [update, history]);

  useEffect(() => {
    listarUsuarios();
  }, []);

  const UserActions = ({ data }) => {
    let component;
    console.log(data.status);
    if (data.status === true) {
      return (
        <Button
          alt="Inativar"
          size="sm"
          className="text-danger"
          color="link"
          onClick={() => toggleModal(data)}
        >
          <BsFillLockFill />
        </Button>
      );
    } else {
      return (
        <Button
          alt="Ativar"
          size="sm"
          className="text-danger"
          color="link"
          onClick={() => toggleModal(data)}
        >
          <BsFillUnlockFill />
        </Button>
      );
    }
  };

  const ModalContent = ({ data }) => {
    const word = data.status ? 'Inativar' :  'Ativar';
      return (
        <>
          <ModalHeader toggle={toggleModal}>{word} usuário</ModalHeader>
          <ModalBody>
            Deseja {word} o Usuário{" "}
            <strong>{data?.name?.split(" ")[0]}</strong> ?
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={() => alterarStatus(data)}>
              SIM
            </Button>
            <Button color="secondary" onClick={toggleModal}>
              NÃO
            </Button>
          </ModalFooter>
        </>
      );

    return "";
  };

  const UserTable = () => {
    if (usuarios && usuarios.length > 0) {
      return (
        <div>
          <Table responsive reverse>
            <thead style={{ color: "#053566" }}>
              <th>Nome</th>
              <th>Email</th>
              <th>Perfil</th>
              <th>Ações</th>
            </thead>
            <tbody>
              {usuarios &&
                usuarios.map((v, i) => (
                  <tr key={i}>
                    <td>{v.name}</td>
                    <td>{v.email}</td>
                    <td>{v.profileName}</td>
                    <td>
                      <UserActions data={v} />
                    </td>
                  </tr>
                ))}
            </tbody>
          </Table>

          <Modal isOpen={modal.isOpen} toggle={toggleModal}>
            <ModalContent data={modal.data} />
          </Modal>
        </div>
      );
    } else {
      return <div>Não existem usuários cadastrados</div>;
    }
  };

  return (
    <div>
      <Title name="Usuários"></Title>
      <div className={"addContainer"}>
        <Button
          onClick={()=> history.push('/sgi/usuarios/adicionar')}
          style={{ backgroundColor: "#053566" }}
        >
          <BsPersonPlusFill /> Adicionar Usuário
        </Button>
      </div>
      <div>{loading ? <Loading /> : <UserTable />}</div>
    </div>
  );
};

export default Usuarios;
