import React, { useState, useEffect, useCallback } from "react";
import {
  BsFillLockFill,
  BsFillUnlockFill,
  BsPersonPlusFill,
  BsPersonLinesFill,
} from "react-icons/bs";
import Loading from "../../components/loading";
import {
  Table,
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Form,
  FormGroup,
  Input,
  Label,
  Card,
  CardHeader,
  CardBody,
  Col,
  Row
} from "reactstrap";
import Title from "../../components/layout/title";
import { createUser } from "../../services/bff.service";
import ReactSwal from "../../plugins/swal";
import {Sign} from '../../assets/styled';
import history from '../../config/history'

const UserForm = (props) => {
  
  const initalFormState = {
    name: "",
    email: "",
    profileId: 0,
  };
  const [form, setForm] = useState(initalFormState);
  const [formState, setFormState] = useState(false)

  const submitForm = () => {
    const nform = {
      ...form,
      data: form,
    };

    createUser(nform)
      .then(() => {
        setFormState(false)
        ReactSwal.fire({
          icon: "success",
          title: `Usuário ${form.name} cadastrado com sucesso! 
          Em breve ele receberá um e-mail com suas respectivas credencias`,

          showConfirmButton: false,
          showCloseButton: true,
          onClose: history.goBack()
        });
        setForm(initalFormState);
      })
      .catch((erro) => {
        if(erro.response.status === 403){
          history.push('/error/403')
        }else{
          ReactSwal.fire({
            icon: "error",
            title: `Erro ao cadastrar usuário`,
            showCloseButton: true,
          });
        }
        
      });
  };

  const validateFormState = () => {
    setFormState(
      form.name.length > 0 && form.email.length > 0 && form.profileId !== 0
    );
  };

  const handleChange = (props) => {
    const { value, name } = props.target;
    setForm({
      ...form,
      [name]: value,
    });
    validateFormState();
  };

  return (
      <div>
    <Title name="Usuários"></Title>
    <Sign>
      <Col sm={12} md={6} lg={6}>
        <Card>
          <CardHeader tag="h4" className="text-center">
            Dados do usuário
          </CardHeader>
          <CardBody>
            <Form>
              <FormGroup>
                <Label for="name">Nome:</Label>
                <Input
                  type="text"
                  name="name"
                  id="name"
                  onChange={handleChange}
                  value={form.name}
                  placeholder="Informe o nome"
                />
              </FormGroup>
              <FormGroup>
                <Label for="name">E-mail:</Label>
                <Input
                  type="email"
                  name="email"
                  id="email"
                  onChange={handleChange}
                  value={form.email}
                  placeholder="Informe o e-mail"
                />
              </FormGroup>
              <FormGroup>
                <Label for="profileId">Perfil:</Label>
                <Input
                  style={{ width: "100%" }}
                  type="select"
                  name="profileId"
                  id="profileId"
                  value={form.profileId}
                  onChange={handleChange}
                >
                  <option value={0}>Selecione</option>
                  <option value={1}>Gestor</option>
                  <option value={2}>Segurança do Trabalho</option>
                  <option value={3}>Compliance</option>
                  <option value={4}>Supervisor</option>
                </Input>
              </FormGroup>
                <Row>
                <Col sm={12} md={6} lg={6}>
                    <Button color="primary" style={{width: "99%"}} onClick={submitForm}>
                        Adicionar
                    </Button>
                </Col>
                <Col sm={12} md={6} lg={6}>
                    <Button color="secondary" style={{width: "100%"}} onClick={()=> history.goBack()}>
                        Voltar
                    </Button>
                </Col>
                </Row>
            </Form>
          </CardBody>
        </Card>
      </Col>
    </Sign>
    </div>
  );
};

export default UserForm;
