import React, { useState, useEffect, useCallback } from "react";
import { getStandards } from "../../services/bff.service.js";
import CardItem from "../../components/normas/card_item";
import Loading from "../../components/loading";
import styled from "styled-components";
import { Col, Row } from "reactstrap";
import Title from '../../components/layout/title'
import history from "../../config/history";
const Normas = () => {
  const [normas, setNormas] = useState([]);
  const [loading, setLoading] = useState(false);
  // const [hasError, setError] = useState(false)

  const getNormas = useCallback(async () => {
    try {
      setLoading(true);
      const res = await getStandards();
      setNormas(res.data);
      setLoading(false);
    } catch (error) {
      setLoading(false);
      if(error.response.status === 403){
        history.push('/error/403')
      }else{
        console.error(error);
      }
    }
  }, []);

  useEffect(() => {
    getNormas();
  }, [getNormas]);

  const MapearNormas = (normas) =>
  normas.map((item, i) => (
      <Col md="3" xl="3" sm="12" xs="12" key={i} className="mb-4">
        <CardItem item={{ ...item }} />
      </Col>
    ));

  return (
    <div>
      <Title name={"Relatório de incidentes"}></Title>
      <BoxPedidos>{loading ? <Loading /> : MapearNormas(normas)}</BoxPedidos>
    </div>
  );
};

export default Normas;

const BoxPedidos = styled(Row)``;
