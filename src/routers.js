import React from "react";
import {
    Router,
    Switch,
    Route,
    Redirect
} from "react-router-dom";
// config
import { isAuthenticated, getDefaultRoute } from './config/auth'
// layout
import Layout from './components/layout'
// views
import SignIn from './views/auth/signin';
import Pedidos from './views/pedidos/listar-pedidos';
import Detalhes from './views/pedidos/detalhes';
import Normas from './views/normas/listar-normas';
import UserForm from './views/usuarios/cadastrar'
import Usuarios from './views/usuarios/listar-usuarios';
import Error404 from './views/errors/404';
import Error403 from './views/errors/403';
import history from './config/history';

const AdminRoute = ({ ...rest }) => {

    if (!isAuthenticated()) {
        return <Redirect to="/" />
    }
    
    return <Route {...rest} />
}


const Routers = () => {

    return (
        <Router history={history}>
            <Layout nomeDaPagina="IndTextBR">
                <Switch>
                    <Route exact path='/' component={SignIn} />

                    <AdminRoute exact path='/sgi/pedidos'  component={Pedidos} />
                    <AdminRoute exact path='/sgi/usuarios/adicionar' component={UserForm} />
                    <AdminRoute exact path='/sgi/pedidos/detalhes/:id' component={Detalhes} />
                    <AdminRoute exact path='/sgi/usuarios' component={Usuarios} />
                    <AdminRoute exact path='/sgn/normas' component={Normas} />
                    <AdminRoute exact to="/error/404" component={Error404} />
                    <AdminRoute exact to="/error/403" component={Error403} />
                    {/* <Redirect from="*" to="/error/404" /> */}
                </Switch>
            </Layout>
        </Router >
    )

}


export default Routers
