import { saveAuth, loggout } from "../../config/auth";
import { login } from "../../services/bff.service";
import history from "../../config/history";

export const SIGN_IN = "SIGN_IN";
export const ERROR = "ERROR";
export const SIGN_OUT = "SIGN_OUT";

export const signInAction = (data) => {
  return async (dispatch) => {
    login(data)
      .then((res) => {
        const defaultRoute = getDefaultRoute(res.data.user.profile.id);
        
        saveAuth({...res.data, defaultRoute}); 
        dispatch({
          type: SIGN_IN,
          data: {...res.data, defaultRoute},
        });
        history.push(defaultRoute);
      })
      .catch((err) => {
        console.error(err);
        dispatch({
          type: ERROR,
          data: true,
        });
      });

    // mandando informação para o reducer
  };
};

export const authorizeUserWithTokenAction = (data) => {
  return async (dispatch) => {
    dispatch({
      type: SIGN_IN,
      data: data,
    });
    history.push(data.defaultRoute);
  }
} 

export const signOutAction = () => {
  return async (dispatch) => {
    loggout()
    dispatch({
      type: SIGN_OUT
    });
    history.push('/');
  };
}

const getDefaultRoute = (profileId) => {
  switch (profileId){
    case  1:
      return "/sgi/pedidos";
    case 2:
      return  "/sgn/normas";
    
    case 3: 
      return "/sgn/normas"
    case 4:
      return "/sgi/pedidos"

    default: return '/sgi/pedidos';
  }
}
