import { SIGN_IN, ERROR, SIGN_OUT } from "./auth.action";
import { getToken, getUser, getDefaultRoute } from "../../config/auth";

const INITIAL_STATE = {
  loading: false,
  token: getToken() || "",
  usuario: getUser() || {},
  defaultRoute: getDefaultRoute() || "",
  hasError: false,
};

const reducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case SIGN_IN:
      state.token = action.data.token;
      state.usuario = action.data.user;
      state.defaultRoute = action.data.defaultRoute;
      state.hasError = false;
      return state;
    case ERROR:
      state.hasError = true;
      return state;
    case SIGN_OUT:
      state.token = "";
      state.usuario = {};
      state.defaultRoute = "";
      state.hasError = false;
      return state;
    default:
      return state;
  }
};

export default reducer;
