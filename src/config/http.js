import axios from 'axios'; // import da dependencia
import {getToken} from './auth'
// definindo a url da api
const urlApi = 'https://indtextbr-bff.herokuapp.com/v1';
 //process.env.REACT_APP_API;
const token = getToken();
// criando um client http através do AXIOS
const http = axios.create({
    baseURL: urlApi
});


http.interceptors.response.use((response) => {
    return response
}, (err) => {
    return Promise.resolve({err})
})

// Definindo o header padrão da aplicação
http.defaults.headers['content-type'] = 'application/json';
if(token !== false){
    http.defaults.headers['token'] = token;
}

export default http;